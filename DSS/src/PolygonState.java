import java.util.List;

public class PolygonState {
    private List<Polygon2D> polygons;
    private final Image stateImage;

    public PolygonState(List<Polygon2D> polygons, Image stateImage) {
        this.polygons = polygons;
        this.stateImage = stateImage;
    }

    public List<Polygon2D> getPolygons() {
        return this.polygons;
    }

    public Image getStateImage() {
        return stateImage;
    }

    public void setPolygons(List<Polygon2D> polygons) {
        this.polygons = polygons;
    }
}

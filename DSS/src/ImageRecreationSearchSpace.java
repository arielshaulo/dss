import java.util.HashSet;
import java.util.Set;

public class ImageRecreationSearchSpace implements SearchSpace<PolygonState> {
    private final StateMutator<PolygonState> stateMutator;

    public ImageRecreationSearchSpace(StateMutator<PolygonState> stateMutator) {
        this.stateMutator = stateMutator;
    }

    @Override
    public Set<PolygonState> getStateNeighbors(PolygonState polygonState, int amountOfNeighborsToCreate) throws Exception {
        Set<PolygonState> polygonNeighbors = new HashSet<>();
        for (int i = 0; i < amountOfNeighborsToCreate; i++) {
            polygonNeighbors.add(this.stateMutator.mutate(polygonState));
        }
        return polygonNeighbors;
    }

}

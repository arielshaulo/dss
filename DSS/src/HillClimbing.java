import java.util.Optional;
import java.util.Random;
import java.util.Set;

public class HillClimbing<S> implements LocalSearchAlgorithm<S> {
    private final int maxNumIterations;
    private final int maxIterationsWithoutChangeBeforeCreatingRandomState;
    private final Random random;
    private int numOfIterationsWithoutChange;
    private AnalyzedState<S> initialState;
    private int improve = 0;

    public HillClimbing(int maxIterations) {
        this(maxIterations, maxIterations);
    }

    public HillClimbing(int maxIterations, int maxIterationsWithoutChangeBeforeCreatingRandomState) {
        this.maxNumIterations = maxIterations;
        this.maxIterationsWithoutChangeBeforeCreatingRandomState = maxIterationsWithoutChangeBeforeCreatingRandomState;
        this.random = new Random();
    }

    @Override
    public S search(FitnessAnalyzer<S> fitnessAnalyzer, SearchSpace<S> searchSpace, S initialState, int amountOfLocalNeighborsPerState) throws Exception {
        this.initialState = new AnalyzedState<>(initialState, fitnessAnalyzer.calcFitness(initialState));
        AnalyzedState<S> currentState = new AnalyzedState<>(initialState, this.initialState.getFitness());
        this.numOfIterationsWithoutChange = 0;
        for (int i = 0; i < this.maxNumIterations; i++) {
            Set<S> neighbors = searchSpace.getStateNeighbors(currentState.getState(), amountOfLocalNeighborsPerState);
            Optional<AnalyzedState<S>> neighborState = getRandomNeighbor(neighbors, fitnessAnalyzer, this.random.nextInt());
            if (neighborState.isEmpty()) {
                break;
            }
            currentState = this.getUpdateState(fitnessAnalyzer, searchSpace, currentState, neighborState.get());
            System.out.println("epoch: " + i + "  " + "loss: " + currentState.getFitness() + " improvements : " + this.improve);

        }
        return currentState.getState();
    }

    private AnalyzedState<S> getUpdateState(FitnessAnalyzer<S> fitnessAnalyzer, SearchSpace<S> searchSpace, AnalyzedState<S> currentState, AnalyzedState<S> neighborState) {
        if (neighborState.getFitness() < currentState.getFitness()) {
            this.numOfIterationsWithoutChange = 0;
            this.improve++;
            return neighborState;
        }
//        else if (this.numOfIterationsWithoutChange > maxIterationsWithoutChangeBeforeCreatingRandomState) {
//            S randomState = searchSpace.createRandomState();
//            this.numOfIterationsWithoutChange = 0;
//            return new AnalyzedState<>(randomState, fitnessAnalyzer.calcFitness(randomState));
//        }
//        numOfIterationsWithoutChange++;
        return currentState;
    }
}

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PolygonStateMutator<T extends Polygon2D> implements StateMutator<PolygonState> {
    private final int colorStepSize;
    private final int coordinateStepSize;
    private final Image targetImage;
    private final PolygonStateDrawer polygonStateDrawer;
    private final PolygonFactory<T> polygonFactory;

    public PolygonStateMutator(int colorStepSize, int coordinateStepSize, Image targetImage, Class<T> polygonClassType) {
        this.colorStepSize = colorStepSize;
        this.coordinateStepSize = coordinateStepSize;
        this.targetImage = targetImage;
        this.polygonStateDrawer = new PolygonStateDrawer();
        this.polygonFactory = new PolygonFactory<>(targetImage, polygonClassType);
    }

    @Override
    public PolygonState mutate(PolygonState polygonState) throws Exception {
//        PolygonState polygonStateWithIncreaseDecreasePolygon = this.increaseDecreasePolygon(polygonState);
        int randomIndexOfPolygon2DToMutate = new Random().nextInt(polygonState.getPolygons().size());
        Polygon2D polygon2DToMutate = polygonState.getPolygons().get(randomIndexOfPolygon2DToMutate);
        Polygon2D polygon2DToMutateCopy = new Polygon2D(polygon2DToMutate.xpoints, polygon2DToMutate.ypoints, polygon2DToMutate.numberOfCoordinates, polygon2DToMutate.getColor());
        Polygon2D mutatedPolygon2D = hybridMutation(polygon2DToMutateCopy);
        return createMutatedState(polygonState, mutatedPolygon2D, randomIndexOfPolygon2DToMutate);
    }

    private Polygon2D hybridMutation(Polygon2D polygon2DToMutate) throws Exception {
        Polygon2D mutatedPolygon;
        Random random = new Random();
        int randomNumber = random.nextInt(3);
        if (randomNumber == 0 || randomNumber == 1) {
            mutatedPolygon = this.softMutation(polygon2DToMutate);
        } else {
            mutatedPolygon = this.mediumMutation(polygon2DToMutate);
        }
        return mutatedPolygon;
    }

    private Polygon2D mediumMutation(Polygon2D polygon2DToMutate) throws Exception {
        Polygon2D mutatedPolygon2D = new Polygon2D(polygon2DToMutate.xpoints, polygon2DToMutate.ypoints, polygon2DToMutate.numberOfCoordinates, polygon2DToMutate.getColor());
        int red = polygon2DToMutate.getColor().getRed();
        int green = polygon2DToMutate.getColor().getGreen();
        int blue = polygon2DToMutate.getColor().getBlue();
        int alpha = polygon2DToMutate.getColor().getAlpha();
        Random random = new Random();
        int indexOfCoordinate = random.nextInt(polygon2DToMutate.numberOfCoordinates);
        switch (random.nextInt(6)) {
            case 0:
                int mutatedRed = random.nextInt(256);
                mutatedPolygon2D.setColor(new Color(mutatedRed, green, blue, alpha));
                break;
            case 1:
                int mutatedGreen = random.nextInt(256);
                mutatedPolygon2D.setColor(new Color(red, mutatedGreen, blue, alpha));
                break;
            case 2:
                int mutatedBlue = random.nextInt(256);
                mutatedPolygon2D.setColor(new Color(red, green, mutatedBlue, alpha));
                break;
            case 3:
                int mutatedAlpha = random.nextInt(256);
                mutatedPolygon2D.setColor(new Color(red, green, blue, mutatedAlpha));
                break;
            case 4:
                mutatedPolygon2D.xpoints[indexOfCoordinate] = random.nextInt(400);
                break;
            case 5:
                mutatedPolygon2D.ypoints[indexOfCoordinate] = random.nextInt(400);
                break;
        }
        return mutatedPolygon2D;
    }

    private Polygon2D softMutation(Polygon2D polygon2DToMutate) throws Exception {
        Polygon2D mutatedPolygon2D = new Polygon2D(polygon2DToMutate.xpoints, polygon2DToMutate.ypoints, polygon2DToMutate.numberOfCoordinates, polygon2DToMutate.getColor());
        int red = polygon2DToMutate.getColor().getRed();
        int green = polygon2DToMutate.getColor().getGreen();
        int blue = polygon2DToMutate.getColor().getBlue();
        int alpha = polygon2DToMutate.getColor().getAlpha();
        Random random = new Random();
        int indexOfCoordinate = random.nextInt(polygon2DToMutate.numberOfCoordinates);
        switch (random.nextInt(6)) {
            case 0:
                int redValueToBeAdded = (int) (red * (this.colorStepSize / 100.0f));
                int mutatedRed = this.mutateSingleColorElement(red, redValueToBeAdded, 256);
                mutatedPolygon2D.setColor(new Color(mutatedRed, green, blue, alpha));
                break;
            case 1:
                int greenValueToBeAdded = (int) (green * (this.colorStepSize / 100.0f));
                int mutatedGreen = this.mutateSingleColorElement(green, greenValueToBeAdded, 256);
                mutatedPolygon2D.setColor(new Color(red, mutatedGreen, blue, alpha));
                break;
            case 2:
                int blueValueToBeAdded = (int) (blue * (this.colorStepSize / 100.0f));
                int mutatedBlue = this.mutateSingleColorElement(blue, blueValueToBeAdded, 256);
                mutatedPolygon2D.setColor(new Color(red, green, mutatedBlue, alpha));
                break;
            case 3:
                int alphaValueToBeAdded = (int) (alpha * (this.colorStepSize / 100.0f));
                int mutatedAlpha = this.mutateSingleColorElement(alpha, alphaValueToBeAdded, 256);
                mutatedPolygon2D.setColor(new Color(red, green, blue, mutatedAlpha));
                break;
            case 4:
                int coordinateXValueToBeAdded = (int) (mutatedPolygon2D.xpoints[indexOfCoordinate] * (this.colorStepSize / 100.0f));
                mutatedPolygon2D.xpoints[indexOfCoordinate] = this.mutateSingleColorElement(mutatedPolygon2D.xpoints[indexOfCoordinate], coordinateXValueToBeAdded, this.targetImage.getWidth());
                break;
            case 5:
                int coordinateYValueToBeAdded = (int) (mutatedPolygon2D.ypoints[indexOfCoordinate] * (this.colorStepSize / 100.0f));
                mutatedPolygon2D.ypoints[indexOfCoordinate] = this.mutateSingleColorElement(mutatedPolygon2D.ypoints[indexOfCoordinate], coordinateYValueToBeAdded, this.targetImage.getWidth());
                break;
        }
        return mutatedPolygon2D;
    }

    private PolygonState createMutatedState(PolygonState currentPolygonState, Polygon2D mutatedPolygon2D, int indexOfPolygonToMutate) {
        List<Polygon2D> currentPolygonStateCopy = new ArrayList<>(currentPolygonState.getPolygons());
        currentPolygonStateCopy.set(indexOfPolygonToMutate, mutatedPolygon2D);
        List<Polygon2D> mutatedPolygon2DList = new ArrayList<>(currentPolygonStateCopy);
        Image mutatedStateImage = this.polygonStateDrawer.drawStateToImage(mutatedPolygon2DList, 400, 400);
        return new PolygonState(mutatedPolygon2DList, mutatedStateImage);
    }

    private int mutateSingleColorElement(int elementValue, double StepSize, int upperRange) {
        int randomNumber = new Random().nextInt(2);
        if ((randomNumber == 0) && (elementValue + StepSize < upperRange)) {
            elementValue += StepSize;
        } else if (elementValue - StepSize >= 0) {
            elementValue -= StepSize;
        } else {
            elementValue += StepSize;
        }
        return elementValue;
    }

    private PolygonState increaseDecreasePolygon(PolygonState polygonState) throws Exception {
        PolygonState newPolygonState = new PolygonState(polygonState.getPolygons(), polygonState.getStateImage());
        int randomNumber = new Random().nextInt(3);
        if (randomNumber == 1 && polygonState.getPolygons().size() < 100) {
            Polygon2D polygonToBeAdded = this.polygonFactory.createXNumbersOfPolygons(1).get(0);
            newPolygonState.getPolygons().add(polygonToBeAdded);
        } else if (randomNumber == 2 && polygonState.getPolygons().size() > 20) {
            newPolygonState.getPolygons().remove(newPolygonState.getPolygons().size() - 1);
            newPolygonState.setPolygons(newPolygonState.getPolygons());
        }
        return newPolygonState;
    }

}

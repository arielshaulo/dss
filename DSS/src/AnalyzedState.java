class AnalyzedState<S> {
    private S key;
    private double value;

    public AnalyzedState(S key, double value) {
        this.key = key;
        this.value = value;
    }

    public S getState() {
        return this.key;
    }

    public double getFitness() {
        return this.value;
    }

}

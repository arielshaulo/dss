import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageUtil {
    String imagePath;
    BufferedImage originalPicture;

    public ImageUtil(String imagePath) {
        this.imagePath = imagePath;
        try {
            this.originalPicture = ImageIO.read(new File(imagePath));
        } catch (IOException error) {
            System.out.println("can not read image");
        }
    }

}

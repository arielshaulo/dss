public class ImageRecreationFitnessAnalyzer implements FitnessAnalyzer<PolygonState> {
    private final Image targetImage;

    public ImageRecreationFitnessAnalyzer(Image targetImage) {
        this.targetImage = targetImage;
    }

    @Override
    public double calcFitness(PolygonState state) {
        final double[] fitness = new double[2];
        Thread t1 = new Thread(() -> fitness[0] = compareImageToTargetImage(0, 0, (state.getStateImage().getWidth() / 2), state.getStateImage().getHeight(), state.getStateImage()));
        Thread t2 = new Thread(() -> fitness[1] = compareImageToTargetImage((state.getStateImage().getWidth() / 2), 0, state.getStateImage().getWidth(), state.getStateImage().getHeight(), state.getStateImage()));
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (Exception exception) {
            System.out.println("לא טוב");
        }

        return fitness[0] + fitness[1];
    }

    private double compareImageToTargetImage(int startX, int startY, int endX, int endY, Image currentStateImage) {
        long difference = 0;
        for (int x = startX; x < endX; x++) {
            for (int y = startY; y < endY; y++) {
                Pixel newColor = currentStateImage.getPixel(x, y);
                Pixel oldColor = this.targetImage.getPixel(x, y);
                double redDiff = Math.abs(newColor.getRed() - oldColor.getRed());
                double greenDiff = Math.abs(newColor.getGreen() - oldColor.getGreen());
                double blueDiff = Math.abs(newColor.getBlue() - oldColor.getBlue());
//                difference += Math.sqrt((redDiff * redDiff) + (greenDiff * greenDiff) + (blueDiff * blueDiff));
                difference += (redDiff) + (greenDiff) + (blueDiff);
            }
        }
        return difference;
    }

}

import java.awt.*;
import java.awt.image.BufferedImage;

public class main {
    public static void main(String[] args) throws Exception {
        Manager manager = new Manager("C:\\Users\\ArielS\\Desktop\\mona400x400.jpg", 100, Triangle.class);
        manager.start();
//        System.out.println("************************");
//        BufferedImage bufferedBestImage = new ImageUtil("C:\\Users\\ArielS\\Desktop\\dssTest\\bestTest.jpg").originalPicture;
//        BufferedImage bufferedTargetImage = new ImageUtil("C:\\Users\\ArielS\\Desktop\\mona400x400.jpg").originalPicture;
//        double d = compareImageToTargetImage(copyBufferedTargetImageToImage(bufferedBestImage), copyBufferedTargetImageToImage(bufferedTargetImage));
//        long maxDiff = 400L * 400 * 3 * 255 ;
//        System.out.println((1 - d / maxDiff) * 100);
    }

    private static double compareImageToTargetImage(Image currentStateImage, Image targetImage) {
        long difference = 0;
        for (int x = 0; x < currentStateImage.getWidth(); x++) {
            for (int y = 0; y < currentStateImage.getHeight(); y++) {
                Pixel newColor = currentStateImage.getPixel(x, y);
                Pixel oldColor = targetImage.getPixel(x, y);
                double redDiff = newColor.getRed() - oldColor.getRed();
                double greenDiff = newColor.getGreen() - oldColor.getGreen();
                double blueDiff = newColor.getBlue() - oldColor.getBlue();
//                difference += (redDiff * redDiff) + (greenDiff * greenDiff) + (blueDiff * blueDiff);
                difference += (redDiff) + (greenDiff) + (blueDiff);
            }
        }
        return difference;
    }

    private static Image copyBufferedTargetImageToImage(BufferedImage targetBufferedImage) {
        Image targetImage = new Image(targetBufferedImage.getWidth(), targetBufferedImage.getHeight());
        for (int x = 0; x < targetBufferedImage.getWidth(); x++) {
            for (int y = 0; y < targetBufferedImage.getHeight(); y++) {
                Color color = new Color(targetBufferedImage.getRGB(x, y));
                Pixel pixel = new Pixel(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
                targetImage.setPixel(x, y, pixel);
            }
        }
        return targetImage;
    }

}
import java.util.Set;

public interface SearchSpace<S> {
    Set<S> getStateNeighbors(S state, int amountOfNeighborsToCreate) throws Exception;

}

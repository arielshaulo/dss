import java.awt.*;

public class Polygon2D extends Polygon {
    private Color color;
    public int numberOfCoordinates;

    public Polygon2D(int[] xPoints, int[] yPoints, int numberOfCoordinates) throws Exception {
        super(xPoints, yPoints, xPoints.length);
        if (!validateXPointsAndYPointsLength(xPoints, yPoints)) {
            throw new Exception();
        }
        this.numberOfCoordinates = numberOfCoordinates;
    }

    public Polygon2D(int[] xPoints, int[] yPoints, int numberOfCoordinates, Color color) throws Exception {
        super(xPoints, yPoints, xPoints.length);
        if (!validateXPointsAndYPointsLength(xPoints, yPoints)) {
            throw new Exception();
        }
        this.numberOfCoordinates = numberOfCoordinates;
        this.color = color;
    }

    protected boolean validateXPointsAndYPointsLength(int[] xPoints, int[] yPoints) {
        return xPoints.length == yPoints.length;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Rectangle calculateBoundingBox() {
        return this.getBounds();
    }

}

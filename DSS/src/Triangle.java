import java.awt.*;

public class Triangle extends Polygon2D {
    public static final int NUMBER_OF_COORDINATES = 3;

    public Triangle(int[] xPoints, int[] yPoints) throws Exception {
        super(xPoints, yPoints, NUMBER_OF_COORDINATES);
        if (!validateXPointsAndYPointsLengthForTriangle(xPoints, yPoints)) {
            throw new Exception();
        }
    }

    public Triangle(int[] xPoints, int[] yPoints, Color color) throws Exception {
        super(xPoints, yPoints, NUMBER_OF_COORDINATES, color);
        if (!validateXPointsAndYPointsLengthForTriangle(xPoints, yPoints)) {
            throw new Exception();
        }
    }

    protected boolean validateXPointsAndYPointsLengthForTriangle(int[] xPoints, int[] yPoints) {
        return xPoints.length == yPoints.length && xPoints.length == 3;
    }
}

public interface StateMutator<S> {
    S mutate(S state) throws Exception;
}

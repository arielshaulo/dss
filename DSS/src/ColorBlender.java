import java.awt.*;

public class ColorBlender {

    public ColorBlender() {
    }

    public Color colorBlend(Color firstColor, Color secondColor) {
        double totalAlpha = firstColor.getAlpha() + secondColor.getAlpha();
        double weight0 = firstColor.getAlpha() / totalAlpha;
        double weight1 = secondColor.getAlpha() / totalAlpha;

        double r = weight0 * firstColor.getRed() + weight1 * secondColor.getRed();
        double g = weight0 * firstColor.getGreen() + weight1 * secondColor.getGreen();
        double b = weight0 * firstColor.getBlue() + weight1 * secondColor.getBlue();
        double a = Math.max(firstColor.getAlpha(), secondColor.getAlpha());

        return new Color((int) r, (int) g, (int) b, (int) a);
    }

}

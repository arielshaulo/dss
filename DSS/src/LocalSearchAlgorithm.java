import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

public interface LocalSearchAlgorithm<S> {
    S search(FitnessAnalyzer<S> fitnessAnalyzer, SearchSpace<S> searchSpace, S initialState, int amountOfLocalNeighborsPerState) throws Exception;

    default Optional<AnalyzedState<S>> getRandomNeighbor(Set<S> neighbors, FitnessAnalyzer<S> fitnessAnalyzer, int seed) {
        if (neighbors.isEmpty()) {
            return Optional.empty();
        }
        S neighbor = chooseRandom(new ArrayList<>(neighbors), seed);
        return Optional.of(new AnalyzedState<>(neighbor, fitnessAnalyzer.calcFitness(neighbor)));
    }

    default S chooseRandom(ArrayList<S> neighbors, int seed){
        Random random = new Random(seed);
        int randomIndex = random.nextInt(neighbors.size());
        return neighbors.get(randomIndex);
    }

}

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class Manager<T extends Polygon2D> {
    private final BufferedImage targetImage;
    private final int numOfPolygons;
    private final PolygonFactory<T> polygonFactory;
    private final SimulatedAnnealing<PolygonState> simulatedAnnealing;
    private final SearchSpace<PolygonState> searchSpace;

    public Manager(String originalImagePath, int numOfPolygons, Class<T> polygonClassType) throws Exception {
        this.targetImage = new ImageUtil(originalImagePath).originalPicture;
        this.numOfPolygons = numOfPolygons;
        this.polygonFactory = new PolygonFactory<>(copyBufferedTargetImageToImage(), polygonClassType);
        StateMutator<PolygonState> stateMutator = new PolygonStateMutator<>(10, 10, copyBufferedTargetImageToImage(), polygonClassType);
        this.searchSpace = new ImageRecreationSearchSpace(stateMutator);
        this.simulatedAnnealing = new SimulatedAnnealing<>(10000, 0, 0.99, 62);
    }

    public void start() throws Exception {
        PolygonStateDrawer polygonStateDrawer = new PolygonStateDrawer();
        List<Polygon2D> polygonList = this.polygonFactory.createXNumbersOfPolygons(numOfPolygons);
        Image initialImage = polygonStateDrawer.drawStateToImage(polygonList, this.targetImage.getWidth(), this.targetImage.getHeight());
        PolygonState initialState = new PolygonState(polygonList, initialImage);
        FitnessAnalyzer<PolygonState> fitnessAnalyzer = new ImageRecreationFitnessAnalyzer(copyBufferedTargetImageToImage());
        PolygonState bestState = this.simulatedAnnealing.search(fitnessAnalyzer, this.searchSpace, initialState, 1);
        Image bestImage = polygonStateDrawer.drawStateToImage(bestState.getPolygons(), this.targetImage.getWidth(), this.targetImage.getHeight());
        saveBestBufferedImageToFile(bestImage);
    }

    public void saveBestBufferedImageToFile(Image Image) throws IOException {
        File outPutFile = new File("C:\\Users\\ArielS\\Desktop\\dssTest\\bestTest.jpg");
        ImageIO.write(copyImageToBufferedImage(Image), "jpg", outPutFile);
    }

    private BufferedImage copyImageToBufferedImage(Image image) {
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(), image.getHeight(), this.targetImage.getType());
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                Pixel pixel = image.getPixel(x, y);
                Color color = new Color(pixel.getRed(), pixel.getGreen(), pixel.getBlue(), pixel.getAlpha());
                bufferedImage.setRGB(x, y, color.getRGB());
            }
        }
        return bufferedImage;
    }

    private Image copyBufferedTargetImageToImage() {
        Image targetImage = new Image(this.targetImage.getWidth(), this.targetImage.getHeight());
        for (int x = 0; x < this.targetImage.getWidth(); x++) {
            for (int y = 0; y < this.targetImage.getHeight(); y++) {
                Color color = new Color(this.targetImage.getRGB(x, y));
                Pixel pixel = new Pixel(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
                targetImage.setPixel(x, y, pixel);
            }
        }
        return targetImage;
    }

}

import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

public class SimulatedAnnealing<S> implements LocalSearchAlgorithm<S> {
    private final Random random;
    private final int maxNumIteration;
    private final double initialTemperature;
    private final double coolingRate;
    private double temperature;
    private AnalyzedState<S> bestState;
    private AnalyzedState<S> currentState;
    private int improve = 0;
    private long maxDiff = 400L * 400 * 3 * 255;

    public SimulatedAnnealing(int maxNumIteration, double initialTemperature, double coolingRate, long seed) {
        this.maxNumIteration = maxNumIteration;
        this.initialTemperature = initialTemperature;
        this.coolingRate = coolingRate;
        this.random = new Random(seed);
    }

    @Override
    public S search(FitnessAnalyzer<S> fitnessAnalyzer, SearchSpace<S> searchSpace, S initialState, int amountOfLocalNeighborsPerState) throws Exception {
        long startTime = System.nanoTime();

        long endTime = System.nanoTime();
        this.bestState = new AnalyzedState<>(initialState, fitnessAnalyzer.calcFitness(initialState));
        this.currentState = bestState;
        this.temperature = initialTemperature;
//        System.out.println("start");
        for (int i = 0; i < this.maxNumIteration; i++) {
            Set<S> neighbors = searchSpace.getStateNeighbors(this.currentState.getState(), amountOfLocalNeighborsPerState);
            Optional<AnalyzedState<S>> neighbor = getRandomNeighbor(neighbors, fitnessAnalyzer, this.random.nextInt());
            if (neighbor.isEmpty()) {
                break;
            }
            this.updateBestStateAndCurrentState(neighbor.get());
            this.updateTemperatureRule();
            System.out.println("epoch: " + i + "  " + "acc: " + (1 - this.currentState.getFitness() / maxDiff) * 100 + " best acc: " + (1 - this.bestState.getFitness() / maxDiff) * 100 + "  improvements :" + this.improve);
        }
//        System.out.println("finish");
        return this.bestState.getState();
    }

    private void updateBestStateAndCurrentState(AnalyzedState<S> neighbor) {
        if (neighbor.getFitness() < this.currentState.getFitness()) {
            this.currentState = neighbor;
            if (neighbor.getFitness() < this.bestState.getFitness()) {
                this.improve++;
                this.bestState = neighbor;
            }
        }
//        else if (this.probabilityOfNewSolution(neighbor) > random.nextDouble()) {
//            this.currentState = neighbor;
//        }
    }

    private double probabilityOfNewSolution(AnalyzedState<S> neighbor) {
        return Math.exp(this.calculateLoss(neighbor) / this.temperature);
    }

    private double calculateLoss(AnalyzedState<S> neighbor) {
        return this.currentState.getFitness() - neighbor.getFitness();
    }

    private void updateTemperatureRule() {
        this.temperature = this.temperature * this.coolingRate;

    }

}

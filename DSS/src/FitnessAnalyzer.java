public interface FitnessAnalyzer<S> {
    double calcFitness(S state);
}

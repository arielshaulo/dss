import java.awt.*;
import java.lang.reflect.Field;
import java.util.Random;

public class PolygonFactoryUtil {
    private final Image targetImage;
    private final Class<?> type;

    public PolygonFactoryUtil(Image targetImage, Class<?> type) {
        this.targetImage = targetImage;
        this.type = type;
    }

    public Coordinate[] generateCoordinates(int numberOfCoordinateToCreate) {
        Coordinate[] coordinates = new Coordinate[numberOfCoordinateToCreate];
        for (int i = 0; i < numberOfCoordinateToCreate; i++) {
            int x = generateNumber(0, this.targetImage.getWidth() - 1);
            int y = generateNumber(0, this.targetImage.getHeight() - 1);
            coordinates[i] = new Coordinate(x, y);
        }
        return coordinates;
    }

    public Color generateColor(Coordinate[] coordinates) {
        ColorBlender colorBlender = new ColorBlender();
        Color blendedColor = null;
        for (Coordinate coordinate : coordinates) {
            Pixel pixel = this.targetImage.getPixel(coordinate.getX(), coordinate.getY());
            Color color = new Color(pixel.getRed(), pixel.getGreen(), pixel.getBlue(), pixel.getAlpha());
            if (blendedColor == null) {
                blendedColor = color;
                continue;
            }
            blendedColor = colorBlender.colorBlend(color, blendedColor);
        }
        return blendedColor;
    }

    private int generateNumber(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    public int getNumberOfCoordinatesToCreateFromField() throws Exception {
        int numberOfCoordinates = 0;
        Field field = this.type.getField("NUMBER_OF_COORDINATES");
        Class<?> t = field.getType();
        if (t == int.class) {
            numberOfCoordinates = field.getInt(null);
        }
        return numberOfCoordinates;
    }
}

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class PolygonStateDrawer {

    public Image drawStateToImage(List<Polygon2D> polygons, int width, int height) {
        BufferedImage image = new BufferedImage(width, height, 5);
        Graphics2D graphics2D = image.createGraphics();
//        graphics2D.setColor(Color.WHITE);
//        graphics2D.fillRect(0,0,width,height);
        for (Polygon2D polygon2D : polygons) {
            graphics2D.setColor(polygon2D.getColor());
            graphics2D.fillPolygon(polygon2D);
        }
//        saveBufferedImageToFile(image);

        return this.copyBufferedTargetImageToImage(image);
    }

    private Image copyBufferedTargetImageToImage(BufferedImage image) {
        final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        final int width = image.getWidth();
        final int height = image.getHeight();
        final boolean hasAlphaChannel = image.getAlphaRaster() != null;
        Pixel[][] result = new Pixel[height][width];
        if (hasAlphaChannel) {
            final int pixelLength = 4;
            for (int pixel = 0, row = 0, col = 0; pixel + 3 < pixels.length; pixel += pixelLength) {
                int argb = 0;
                argb += (((int) pixels[pixel] & 0xff) << 24);
                argb += ((int) pixels[pixel + 1] & 0xff);
                argb += (((int) pixels[pixel + 2] & 0xff) << 8);
                argb += (((int) pixels[pixel + 3] & 0xff) << 16);
                Color color = new Color(argb);
                result[row][col] = new Pixel(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
                col++;
                if (col == width) {
                    col = 0;
                    row++;
                }
            }
        } else {
            final int pixelLength = 3;
            for (int pixel = 0, row = 0, col = 0; pixel + 2 < pixels.length; pixel += pixelLength) {
                int argb = 0;
                argb += -16777216; // 255 alpha
                argb += ((int) pixels[pixel] & 0xff); // blue
                argb += (((int) pixels[pixel + 1] & 0xff) << 8); // green
                argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red
                Color color = new Color(argb);
                result[row][col] = new Pixel(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
                col++;
                if (col == width) {
                    col = 0;
                    row++;
                }
            }
        }

        return new Image(result, width, height);
    }

    private Color convertColorFromPixel(Pixel pixel) {
        return new Color(pixel.getRed(), pixel.getGreen(), pixel.getBlue(), pixel.getAlpha());

    }

    private Pixel convertPixelFromColor(Color color) {
        return new Pixel(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    public void saveBufferedImageToFile(BufferedImage Image) throws IOException {
        File outPutFile = new File("C:\\Users\\ArielS\\Desktop\\dssTest\\test.png");
        ImageIO.write(Image, "png", outPutFile);

    }

    public Image drawStateToImageByRaz(List<Polygon2D> polygons, int width, int height) {
        Image image = new Image(width, height);
        for (Polygon2D polygon2D : polygons) {
            drawPolygon(polygon2D, image);
        }
        return image;
    }


    private void drawPolygon(Polygon2D polygon2D, Image image) {
        Rectangle boundingBox = polygon2D.calculateBoundingBox();
        ColorBlender colorBlender = new ColorBlender();
        for (int x = boundingBox.x; x < boundingBox.x + boundingBox.width; x++) {
            for (int y = boundingBox.y; y < boundingBox.y + boundingBox.height; y++) {
                if (polygon2D.contains(new Point(x, y))) {
                    Color color = colorBlender.colorBlend(convertColorFromPixel(image.getPixel(x, y)), polygon2D.getColor());
                    image.setPixel(x, y, convertPixelFromColor(color));
                }
            }
        }
    }

//    function drawShape(ctx, shape, color) {
//        ctx.fillStyle = "rgba("+color.r+","+color.g+","+color.b+","+color.a+")";
//        ctx.beginPath();
//        ctx.moveTo(shape[0].x, shape[0].y);
//        for(var i=1;i<ACTUAL_POINTS;i++) {
//            ctx.lineTo(shape[i].x, shape[i].y);
//        }
//        ctx.closePath();
//        ctx.fill();
//    }
//
//    function drawDNA(ctx, dna) {
//        ctx.fillStyle = "rgb(255,255,255)";
//        ctx.fillRect(0, 0, IWIDTH, IHEIGHT);
//        for(var i=0;i<ACTUAL_SHAPES;i++) {
//            drawShape(ctx, dna[i].shape, dna[i].color);
//        }
//    }
}

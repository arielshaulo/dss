public class Image {
    private final Pixel[][] pixels;
    private final int width;
    private final int height;


    public Image(Pixel[][] pixels, int width, int height) {
        this.pixels = pixels;
        this.width = width;
        this.height = height;
    }

    public Image(int width, int height) {
        this.width = width;
        this.height = height;
        this.pixels = new Pixel[width][height];
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                this.pixels[x][y] = new Pixel(0, 0, 0, 0);
            }
        }
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public Pixel getPixel(int x, int y) {
        return this.pixels[x][y];
    }

    public void setPixel(int x, int y, Pixel pixel) {
        this.pixels[x][y] = pixel;
    }

}

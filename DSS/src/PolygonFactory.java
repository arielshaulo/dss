import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PolygonFactory<T extends Polygon2D> {
    private final Class<T> polygonClassType;
    private final PolygonFactoryUtil polygonFactoryUtil;

    public PolygonFactory(Image targetImage, Class<T> polygonClassType) {
        this.polygonClassType = polygonClassType;
        this.polygonFactoryUtil = new PolygonFactoryUtil(targetImage, polygonClassType);
    }

    public Polygon2D createPolygon(Coordinate[] coordinates, Color color) throws Exception {
        Class<?>[] classList = new Class[3];
        classList[0] = int[].class;
        classList[1] = int[].class;
        classList[2] = Color.class;

        int[] xPoints = new int[coordinates.length];
        int[] yPoints = new int[coordinates.length];
        for (int i = 0; i < coordinates.length; i++) {
            xPoints[i] = coordinates[i].getX();
            yPoints[i] = coordinates[i].getY();
        }
        return this.polygonClassType.getDeclaredConstructor(classList).newInstance(xPoints, yPoints, color);
    }

    public List<Polygon2D> createXNumbersOfPolygons(int numberOfPolygonsToCreate) throws Exception {
        List<Polygon2D> polygonList = new ArrayList<>();
        int numberOfCoordinatesToCreate = this.polygonFactoryUtil.getNumberOfCoordinatesToCreateFromField();
        for (int i = 0; i < numberOfPolygonsToCreate; i++) {
            Coordinate[] coordinates = this.polygonFactoryUtil.generateCoordinates(numberOfCoordinatesToCreate);
            Color color = this.polygonFactoryUtil.generateColor(coordinates);
            Polygon2D polygon = createPolygon(coordinates, color);
            polygonList.add(polygon);
        }
        return polygonList;
    }
}
